#include <iostream>
#include <cstring>
#include <ctime>

class TechnicalTask
{
public:
    int number;
    char* description;

    TechnicalTask(int number)
    {
        this->number = number;
        this->addDescription();
    }

    TechnicalTask(const TechnicalTask &other)
    {
        this->number = other.number;
        this->description = new char[strlen(other.description) + 1];

        strcpy(this->description, other.description);
    }

    virtual void getInfo() = 0;

    void addDescription()
    {
        char description[256];

        std::cout << "Please, enter the description of this product: " << std::endl;
        std::cin.getline(description, 255);

        this->description = new char[strlen(description) + 1];

        strcpy(this->description, description);
    }

    TechnicalTask & operator = (const TechnicalTask &other)
    {
        this->number = other.number;

        delete[] this->description;

        this->description = new char[strlen(other.description) + 1];

        strcpy(this->description, other.description);

        return *this;
    }

    ~TechnicalTask()
    {
        delete[] description;
    }
};

class Request
{
public:
    int number;
    char* name;
    int completeDate;
    char* surname;

    Request(int number, char name, int completeDate, char surname)
    {
        this->number = number;
        this->name = new char[strlen(&name) + 1];
        this->completeDate = completeDate;
        this->surname = new char[strlen(&surname) + 1];

        strcpy(this->name, &name);
        strcpy(this->surname, &surname);
    }

    Request(const Request &other)
    {
        this->number = other.number;
        this->name = new char[strlen(other.name) + 1];
        this->completeDate = other.completeDate;
        this->surname = new char[strlen(other.surname) + 1];

        strcpy(this->name, other.name);
        strcpy(this->surname, other.surname);
    }

    virtual void getInfo() = 0;

    void changeName()
    {
        char name[256];

        std::cout << "Please, enter the new name of this request: " << std::endl;
        std::cin.getline(name, 255);

        this->name = new char[strlen(name) + 1];

        strcpy(this->name, name);
    }

    void changeDate()
    {
        std::cout << "Please, enter the new date: " << std::endl;
        std::cin >> this->completeDate;
    }

    Request & operator = (const Request &other)
    {
        this->number = other.number;
        this->completeDate = other.completeDate;

        delete[] name, surname;

        this->name = new char[strlen(other.name) + 1];
        this->surname = new char[strlen(other.surname) + 1];

        strcpy(this->name, other.name);
        strcpy(this->surname, other.surname);

        return *this;
    }

    ~Request()
    {
        delete[] name, surname;
    }
};

class TechnicalOrder : public TechnicalTask, public Request
{
public:
    int work_cost;

    TechnicalOrder(int work_cost = 1000, int number = 0, char name = 's', int completeDate = 2020, char surname = 'o') : TechnicalTask(number), Request(number, name, completeDate, surname)
    {
        this->work_cost = work_cost;
    }

    TechnicalOrder(const TechnicalOrder &other) : TechnicalTask(other), Request(other)
    {
        this->work_cost = other.work_cost;
    }

	friend std::ostream& operator<< (std::ostream &out, const TechnicalOrder &TechnicalOrder);
	friend std::istream& operator>> (std::istream &in, TechnicalOrder &TechnicalOrder);

    void getWorkCost()
    {
        std::cout << "Cost of work on current period: " << this->work_cost << std::endl;
    }

    void changeWorkCost()
    {
        std::cout << "Please, enter the new cost of work: " << std::endl;
        std::cin >> this->work_cost;
    }

    void getInfo() override
    {
        std::cout << "Number of task: " << TechnicalTask::number << std::endl;
        std::cout << "Description of task: " << this->description << std::endl;
        std::cout << "Number of request: " << Request::number << std::endl;
        std::cout << "Name of request: " << this->name << std::endl;
        std::cout << "Complete date of request: " << this->completeDate << std::endl;
        std::cout << "Surname of request: " << this->surname << std::endl;
        std::cout << "Cost of work on current period: " << this->work_cost << std::endl;
    }

    TechnicalOrder & operator = (const TechnicalOrder &other)
    {
        this->work_cost = other.work_cost;
		TechnicalTask::operator =(other);
		Request::operator =(other);

        return *this;
    }

    ~TechnicalOrder(){}
};

std::ostream& operator<< (std::ostream &out, const TechnicalOrder &obj)
{
	out << "Number of task: " << obj.TechnicalTask::number << std::endl;
	out << "Description of task: " << obj.description << std::endl;
	out << "Number of request: " << obj.Request::number << std::endl;
	out << "Name of request: " << obj.name << std::endl;
	out << "Complete date of request: " << obj.completeDate << std::endl;
	out << "Surname of request: " << obj.surname << std::endl;
	out << "Cost of work on current period: " << obj.work_cost << std::endl;
 
    return out;
}

std::istream& operator>> (std::istream &in, TechnicalOrder &obj)
{
	in >> obj.TechnicalTask::number;
	in >> obj.description;
	in >> obj.Request::number;
	in >> obj.name;
	in >> obj.completeDate;
	in >> obj.surname;
	in >> obj.work_cost;

	return in;
}

// The fourth label. Variant 10

template <typename T>
void gnome_sort(T trash[], unsigned int arrayLength)
{
    for(int i = 0; i + 1 < arrayLength; ++i)
    {
        if(trash[i] > trash[i + 1])
        {
            std::swap(trash[i], trash[i + 1]);

            if(i != 0)
            {
                i -= 2;
            }
        }
    }
}

template <typename T>
void printAr(T sortAr[], unsigned int arrayLength)
{
	for(int i = 0; i < arrayLength; i++)
    {
        std::cout << sortAr[i] << std::endl;
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");

	TechnicalOrder A;
	//A.getInfo();
	std::cin >> A;
	std::cout << A;

    /*int trash[13] = {1, 2, 2, 5, 1, 2, 4, 2, 8, 9, 34, 22, 0};
	unsigned int len = sizeof(trash) / sizeof(*trash);

    gnome_sort(trash, len);
	printAr(trash, len);*/

    system("pause");

    return 0;
}
